package com.toyotaroute.model;

import com.toyotaroute.dto.RouteDTO;
import com.toyotaroute.dto.StationDTO;
import com.toyotaroute.dto.SystemUserDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


@Entity
@Table(name = "STATION")
public class Station implements Serializable {

    public Station convert(StationDTO dto) {
        this.setId(dto.getId());
        this.setStationName(dto.getStationName());
        this.setStationNo(dto.getStationNo());
        this.setArrivalTime(dto.getArrivalTime());
        this.setDepartureTime(dto.getDepartureTime());
        this.setCreateUser(new SystemUser().convert(dto.getCreateUser()));
        this.setCreateDate(dto.getCreateDate());
        this.setLastUpdateUser(new SystemUser().convert(dto.getLastUpdateUser()));
        this.setLastUpdateDate(dto.getLastUpdateDate());
        this.setRoute(new Route().convert(dto.getRoute()));
        return this;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ")
    @Column(name = "STATION_ID")
    private int id;

    @Column(name = "STATION_NAME", nullable = false, length = 100)
    private String stationName;

    @Column(name = "STATION_NO", nullable = false)
    private int stationNo;

    @Column(name = "ARRIVAL_TIME", nullable = false, length = 5)
    private String arrivalTime;

    @Column(name = "DEPARTURE_TIME", nullable = false, length = 5)
    private String departureTime;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "LASTUPD_DATE", nullable = false)
    private Date lastUpdateDate;

    @ManyToOne
    @JoinColumn(name = "CREATE_UID", nullable = false)
    private SystemUser createUser;

    @ManyToOne
    @JoinColumn(name = "LASTUPD_UID", nullable = false)
    private SystemUser lastUpdateUser;

    @ManyToOne
    @JoinColumn(name = "ROUTE_ID", nullable = false)
    private Route route;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public int getStationNo() {
        return stationNo;
    }

    public void setStationNo(int stationNo) {
        this.stationNo = stationNo;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public SystemUser getCreateUser() {
        return createUser;
    }

    public void setCreateUser(SystemUser createUser) {
        this.createUser = createUser;
    }

    public SystemUser getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(SystemUser lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Station station = (Station) o;
        return Objects.equals(id, station.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
