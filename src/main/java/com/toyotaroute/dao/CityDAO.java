package com.toyotaroute.dao;

import com.toyotaroute.model.City;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CityDAO extends GenericDAO<City> {
    public CityDAO() {
        super(City.class);
    }
}
