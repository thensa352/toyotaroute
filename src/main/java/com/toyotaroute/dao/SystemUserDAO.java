package com.toyotaroute.dao;

import com.toyotaroute.model.SystemUser;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class SystemUserDAO extends GenericDAO<SystemUser> {
    public SystemUserDAO() {
        super(SystemUser.class);
    }

    //Kullanıcı login olmaya çalıştığında, sorgu yapılıyor.
    public SystemUser login(String username) {

        SystemUser systemUser = null;
        try {
            systemUser = (SystemUser) entityManager.createQuery("SELECT u FROM SystemUser as u WHERE u.username=:username")
                    .setParameter("username", username).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return systemUser;
    }
}
