package com.toyotaroute.service;

import com.toyotaroute.dao.RouteTypeDAO;
import com.toyotaroute.dto.RouteTypeDTO;
import com.toyotaroute.model.City;
import com.toyotaroute.model.RouteType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class RouteTypeService {

    @Autowired
    private RouteTypeDAO routeTypeDAO;

    @Transactional
    public RouteTypeDTO save(RouteTypeDTO dto) {
        RouteType routeType = new RouteType();
        routeType.convert(dto);
        try {
            routeType = routeTypeDAO.persist(routeType);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(routeType);
    }

    @Transactional
    public RouteTypeDTO edit(int id, RouteTypeDTO dto) {
        RouteType routeType = new RouteType();
        try {
            routeType.convert(dto);
            routeType = routeTypeDAO.merge(routeType);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(routeType);
    }

    @Transactional
    public boolean delete(int id) {
        try {
            RouteType routeType = routeTypeDAO.find(id);
            routeTypeDAO.remove(routeType);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public RouteTypeDTO get(int id) {
        RouteType routeType;
        try {
            routeType = routeTypeDAO.find(id);
        } catch (Exception e) {
            return null;
        }
        return new RouteTypeDTO().convert(routeType);
    }

    public List<RouteTypeDTO> getAll() {
        List<RouteTypeDTO> routeTypeDTOList = new ArrayList<RouteTypeDTO>();
        try {
            for (RouteType routeType : routeTypeDAO.findAll())
                routeTypeDTOList.add(new RouteTypeDTO().convert(routeType));
        } catch (Exception e) {
            return null;
        }
        return routeTypeDTOList;
    }
}
