package com.toyotaroute.service;

import com.toyotaroute.dao.StationDAO;
import com.toyotaroute.dto.RouteDTO;
import com.toyotaroute.dto.StationDTO;
import com.toyotaroute.model.Route;
import com.toyotaroute.model.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class StationService {

    @Autowired
    private StationDAO stationDAO;

    @Transactional
    public StationDTO save(StationDTO dto) {
        Station station = new Station();
        station.convert(dto);
        try {
            station = stationDAO.persist(station);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(station);
    }

    @Transactional
    public StationDTO edit(int id, StationDTO dto) {
        Station station = new Station();
        try {
            station.convert(dto);
            stationDAO.merge(station);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(station);
    }

    @Transactional
    public boolean delete(int id) {
        try {
            Station station = stationDAO.find(id);
            stationDAO.remove(station);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public StationDTO get(int id) {
        Station station;
        try {
            station = stationDAO.find(id);
        } catch (Exception e) {
            return null;
        }
        return new StationDTO().convert(station);
    }

    public List<StationDTO> getAll() {
        List<StationDTO> stationDTOList = new ArrayList<StationDTO>();
        try {
            for (Station station : stationDAO.findAll())
                stationDTOList.add(new StationDTO().convert(station));
        } catch (Exception e) {
            return null;
        }
        return stationDTOList;
    }

    public List<StationDTO> search(String cityName, String routeTypeName, String routeName, String totalDuration, int peronNo,
                                   int vehicleType) {
        List<StationDTO> stationDTOList = new ArrayList<StationDTO>();
        try {
            for (Station station : stationDAO.search(cityName, routeTypeName, routeName, totalDuration, peronNo, vehicleType))
                stationDTOList.add(new StationDTO().convert(station));
        } catch (Exception e) {
            return null;
        }
        return stationDTOList;
    }

    public List<StationDTO> searchByAll(String cityName, String routeTypeName, String routeName, String totalDuration, int peronNo,
                                        int vehicleType, String stationName, int stationNo, String arrivalTime, String departureTime) {
        List<StationDTO> stationDTOList = new ArrayList<StationDTO>();
        try {
            for (Station station : stationDAO.searchByAll(cityName, routeTypeName, routeName, totalDuration, peronNo, vehicleType,
                    stationName, stationNo, arrivalTime, departureTime))
                stationDTOList.add(new StationDTO().convert(station));
        } catch (Exception e) {
            return null;
        }
        return stationDTOList;
    }
}
