package com.toyotaroute.resource;

import com.toyotaroute.dto.*;
import com.toyotaroute.service.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.util.*;

@Component
@Path("/export")
public class ExportExcelResource {

    @Autowired
    private CityService cityService;

    @Autowired
    private RouteService routeService;

    @Autowired
    private RouteTypeService routeTypeService;

    @Autowired
    private StationService stationService;

    @Autowired
    private SystemUserService systemUserService;

    @GET
    @Produces("application/vnd.ms-excel")
    public Response ExportExcel() {

        //Verilerin yazılacağı dosya tanımlanıyor.
        final File file = new File("servis-kayıtları.xls");

        //Tarihleri türkçeleştirmek için Locale sınıfından bir nesne oluşturuldu.
        Locale locale = new Locale("tr", "TR");

        //Verilerin kaydedileceği excel çalışma alanı oluşturuluyor.
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Databasedeki tüm veriler çekiliyor.
        List<CityDTO> cityDTOList = cityService.getAll();
        List<RouteDTO> routeDTOList = routeService.getAll();
        List<RouteTypeDTO> routeTypeDTOList = routeTypeService.getAll();
        List<StationDTO> stationDTOList = stationService.getAll();
        List<SystemUserDTO> systemUserDTOList = systemUserService.getAll();

        //Excel sheetlerinin(sayfalarının) ekleneceği bir liste oluşturuluyor.
        List<XSSFSheet> sheets = new ArrayList<XSSFSheet>();

        //Oluşturulmak istenen tüm sayfalar listeye ekleniyor.
        sheets.add(workbook.createSheet("Şehirler"));
        sheets.add(workbook.createSheet("Güzergahlar"));
        sheets.add(workbook.createSheet("Güzergah Tipleri"));
        sheets.add(workbook.createSheet("Duraklar"));
        sheets.add(workbook.createSheet("Sistem Kullanıcıları"));

        //Veritabanından çekilen verilerin keysetlerinin tutulacağı liste tanımlanıyor.
        List<TreeMap<String, Object[]>> datas = new ArrayList<TreeMap<String, Object[]>>();

        for (int i = 0; i < 5; i++)
            datas.add(new TreeMap<String, Object[]>());

        //Başlıklar ekleniyor.
        datas.get(0).put("1", new Object[]{"ID", "Şehir Adı", "Oluşturan Kullanıcı",
                "Oluşturulma Tarihi", "Son Güncelleyen Kullanıcı", "Son Güncellenme Tarihi"});
        //Veriler keysete ekleniyor.
        for (int i = 0; i < cityDTOList.size(); i++) {
            datas.get(0).put(String.valueOf((i + 2)), new Object[]{cityDTOList.get(i).getId(),
                    cityDTOList.get(i).getCityName(),
                    cityDTOList.get(i).getCreateUser().getUsername(),
                    DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM,locale).format(cityDTOList.get(i).getCreateDate()),
                    cityDTOList.get(i).getLastUpdateUser().getUsername(),
                    DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM,locale).format(cityDTOList.get(i).getLastUpdateDate())});
        }

        //Başlıklar ekleniyor.
        datas.get(1).put("1", new Object[]{"ID", "Şehir", "Güzergah Tipi", "Güzergah", "Toplam Süre", "Peron No", "Servis Aracı"
        ,"Oluşturan Kullanıcı", "Oluşturulma Tarihi", "Son Güncelleyen Kullanıcı", "Son Güncellenme Tarihi"});
        //Veriler keysete ekleniyor.
        for (int i = 0; i < routeDTOList.size(); i++) {
            datas.get(1).put(String.valueOf((i + 2)), new Object[]{routeDTOList.get(i).getId(),
                    routeDTOList.get(i).getCity().getCityName(),
                    routeDTOList.get(i).getRouteType().getRouteTypeName(),
                    routeDTOList.get(i).getRouteName(),
                    routeDTOList.get(i).getTotalDuration(),
                    routeDTOList.get(i).getPeronNo(),
                    routeDTOList.get(i).getVehicleType(),
                    routeDTOList.get(i).getCreateUser().getUsername(),
                    DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, locale).format(routeDTOList.get(i).getCreateDate()),
                    routeDTOList.get(i).getLastUpdateUser().getUsername(),
                    DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM,locale).format(routeDTOList.get(i).getLastUpdateDate())});
        }

        //Başlıklar ekleniyor.
        datas.get(2).put("1", new Object[]{"ID", "Güzergah Tipi", "Oluşturan Kullanıcı", "Oluşturulma Tarihi", "Son Güncelleyen Kullanıcı", "Son Güncellenme Tarihi"});
        //Veriler keysete ekleniyor.
        for (int i = 0; i < routeTypeDTOList.size(); i++) {
            datas.get(2).put(String.valueOf((i + 2)), new Object[]{routeTypeDTOList.get(i).getId(),
                    routeTypeDTOList.get(i).getRouteTypeName(),
                    routeTypeDTOList.get(i).getCreateUser().getUsername(),
                    DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, locale).format(routeTypeDTOList.get(i).getCreateDate()),
                    routeTypeDTOList.get(i).getLastUpdateUser().getUsername(),
                    DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, locale).format(routeTypeDTOList.get(i).getLastUpdateDate())});
        }

        //Başlıklar ekleniyor.
        datas.get(3).put("1", new Object[]{"ID", "Şehir", "Güzergah Tipi", "Güzergah", "Durak İsmi", "Durak No", "Varış Saati", "Kalkış Saati",
                "Oluşturan Kullanıcı", "Oluşturulma Tarihi", "Son Güncelleyen Kullanıcı", "Son Güncellenme Tarihi"});
        //Veriler keysete ekleniyor.
        for (int i = 0; i < stationDTOList.size(); i++) {
            datas.get(3).put(String.valueOf((i + 2)), new Object[]{stationDTOList.get(i).getId(),
                    stationDTOList.get(i).getRoute().getCity().getCityName(),
                    stationDTOList.get(i).getRoute().getRouteType().getRouteTypeName(),
                    stationDTOList.get(i).getRoute().getRouteName(),
                    stationDTOList.get(i).getStationName(),
                    stationDTOList.get(i).getStationNo(),
                    stationDTOList.get(i).getDepartureTime(),
                    stationDTOList.get(i).getArrivalTime(),
                    stationDTOList.get(i).getCreateUser().getUsername(),
                    DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, locale).format(stationDTOList.get(i).getCreateDate()),
                    stationDTOList.get(i).getLastUpdateUser().getUsername(),
                    DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, locale).format(stationDTOList.get(i).getLastUpdateDate())});
        }

        //Başlıklar ekleniyor.
        datas.get(4).put("1", new Object[]{"ID", "Adı", "Soyadı", "Kullanıcı Adı", "Şifre"});
        //Veriler keysete ekleniyor.
        for (int i = 0; i < systemUserDTOList.size(); i++) {
            datas.get(4).put(String.valueOf((i + 2)), new Object[]{systemUserDTOList.get(i).getId(),
                    systemUserDTOList.get(i).getFirstName(),
                    systemUserDTOList.get(i).getLastName(),
                    systemUserDTOList.get(i).getUsername(),
                    systemUserDTOList.get(i).getPassword()});
        }

        //Bütün keyset listesi dolaşılarak içindeki veriler excel dosyasına yazılıyor.
        for (int i = 0; i < sheets.size(); i++) {
            Set<String> keyset = datas.get(i).keySet();
            int rownum = 0;
            for (String key : keyset) {
                Row row = sheets.get(i).createRow(rownum++);
                Object[] objArr = datas.get(i).get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum++);
                    sheets.get(i).autoSizeColumn(cell.getColumnIndex());
                    if (obj instanceof String)
                        cell.setCellValue((String) obj);
                    else if (obj instanceof Integer)
                        cell.setCellValue((Integer) obj);
                }
            }
        }

        try {
            //Tanımlanan dosya için diske erişim sağlanıyor.
            FileOutputStream out = new FileOutputStream(file);
            //Veriler dosyaya yazılıyor.
            workbook.write(out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Başarılı cevabı gönderiliyor ve dosya response'un entity'sine set ediliyor.
        Response.ResponseBuilder responseBuilder = Response.ok(file);
        //Kullanıcıya gönderilen dosya ismi belirleniyor.
        responseBuilder.header("Content-Disposition", "attachment; filename=servis-kayitlari.xls");

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                file.delete();
            }
        }, 100000);

        return responseBuilder.build();
    }
}
