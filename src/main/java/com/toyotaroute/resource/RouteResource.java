package com.toyotaroute.resource;

import com.toyotaroute.dto.CityDTO;
import com.toyotaroute.dto.RouteDTO;
import com.toyotaroute.security.CustomAuthenticationProvider;
import com.toyotaroute.service.CityService;
import com.toyotaroute.service.RouteService;
import com.toyotaroute.service.RouteTypeService;
import com.toyotaroute.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;


@Component
@Path("/routes")
public class RouteResource {

    @Autowired
    private RouteService routeService;

    @Autowired
    private SystemUserService systemUserService;

    @Autowired
    private CityService cityService;

    @Autowired
    private RouteTypeService routeTypeService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RouteDTO save(RouteDTO dto) {
        dto.setCreateUser(systemUserService.login(CustomAuthenticationProvider.getUsername()));
        dto.setLastUpdateUser(dto.getCreateUser());
        dto.setCreateDate(new Date());
        dto.setLastUpdateDate(new Date());
        dto.setCity(cityService.get(dto.getCity().getId()));
        dto.setRouteType(routeTypeService.get(dto.getRouteType().getId()));
        return routeService.save(dto);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RouteDTO edit(@PathParam("id") int id, RouteDTO dto) {
        dto.setLastUpdateUser(systemUserService.login(CustomAuthenticationProvider.getUsername()));
        dto.setLastUpdateDate(new Date());
        dto.setCity(cityService.get(dto.getCity().getId()));
        dto.setRouteType(routeTypeService.get(dto.getRouteType().getId()));
        return routeService.edit(id, dto);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public int delete(@PathParam("id") int id) {
        routeService.delete(id);
        return id;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public RouteDTO get(@PathParam("id") int id) {
        return routeService.get(id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<RouteDTO> getAll() {
        return routeService.getAll();
    }

    @GET
    @Path("/search/{cityName}/{routeTypeName}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<RouteDTO> search(@PathParam("cityName") String cityName,@PathParam("routeTypeName") String routeTypeName) {
        List<RouteDTO> result = routeService.search(cityName,routeTypeName);
        return result;
    }
}
