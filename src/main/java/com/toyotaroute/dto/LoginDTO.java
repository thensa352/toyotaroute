package com.toyotaroute.dto;

public class LoginDTO {
    private String username;

    private String firstName;

    private String lastName;

    private boolean status;//Şifrenin yanlış girilip girilmediğini ve hata mesajının gösterimi için tanımlandı

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
