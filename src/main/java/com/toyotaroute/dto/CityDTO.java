package com.toyotaroute.dto;

import com.toyotaroute.model.City;
import com.toyotaroute.model.Route;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class CityDTO implements Serializable {

    public CityDTO convert(City entity) {
        this.setId(entity.getId());
        this.setCityName(entity.getCityName());
        this.setCreateUser(new SystemUserDTO().convert(entity.getCreateUser()));
        this.setCreateDate(entity.getCreateDate());
        this.setLastUpdateUser(new SystemUserDTO().convert(entity.getLastUpdateUser()));
        this.setLastUpdateDate(entity.getLastUpdateDate());
        return this;
    }

    private int id;

    private String cityName;

    private SystemUserDTO createUser;

    private SystemUserDTO lastUpdateUser;

    private Date createDate;

    private Date lastUpdateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public SystemUserDTO getCreateUser() {
        return createUser;
    }

    public void setCreateUser(SystemUserDTO createUser) {
        this.createUser = createUser;
    }

    public SystemUserDTO getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(SystemUserDTO lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityDTO cityDTO = (CityDTO) o;
        return Objects.equals(id, cityDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
