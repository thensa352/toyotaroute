package com.toyotaroute.dto;

import com.toyotaroute.model.Station;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


public class StationDTO implements Serializable {

    public StationDTO convert(Station entity) {
        this.setId(entity.getId());
        this.setStationName(entity.getStationName());
        this.setStationNo(entity.getStationNo());
        this.setArrivalTime(entity.getArrivalTime());
        this.setDepartureTime(entity.getDepartureTime());
        this.setCreateUser(new SystemUserDTO().convert(entity.getCreateUser()));
        this.setCreateDate(entity.getCreateDate());
        this.setLastUpdateUser(new SystemUserDTO().convert(entity.getLastUpdateUser()));
        this.setLastUpdateDate(entity.getLastUpdateDate());
        this.setRoute(new RouteDTO().convert(entity.getRoute()));
        return this;
    }

    private int id;

    private String stationName;

    private int stationNo;

    private String arrivalTime;

    private String departureTime;

    private Date createDate;

    private Date lastUpdateDate;

    private SystemUserDTO createUser;

    private SystemUserDTO lastUpdateUser;

    private RouteDTO route;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public int getStationNo() {
        return stationNo;
    }

    public void setStationNo(int stationNo) {
        this.stationNo = stationNo;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public SystemUserDTO getCreateUser() {
        return createUser;
    }

    public void setCreateUser(SystemUserDTO createUser) {
        this.createUser = createUser;
    }

    public SystemUserDTO getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(SystemUserDTO lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public RouteDTO getRoute() {
        return route;
    }

    public void setRoute(RouteDTO route) {
        this.route = route;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StationDTO station = (StationDTO) o;
        return Objects.equals(id, station.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
