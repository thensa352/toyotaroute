package com.toyotaroute.dto;

import com.toyotaroute.model.Route;
import com.toyotaroute.model.RouteType;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class RouteTypeDTO implements Serializable {

    public RouteTypeDTO convert(RouteType entity) {
        this.setId(entity.getId());
        this.setRouteTypeName(entity.getRouteTypeName());
        this.setCreateUser(new SystemUserDTO().convert(entity.getCreateUser()));
        this.setCreateDate(entity.getCreateDate());
        this.setLastUpdateUser(new SystemUserDTO().convert(entity.getLastUpdateUser()));
        this.setLastUpdateDate(entity.getLastUpdateDate());
        return this;
    }

    private int id;

    private String routeTypeName;

    private SystemUserDTO createUser;

    private SystemUserDTO lastUpdateUser;

    private Date createDate;

    private Date lastUpdateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRouteTypeName() {
        return routeTypeName;
    }

    public void setRouteTypeName(String routeTypeName) {
        this.routeTypeName = routeTypeName;
    }

    public SystemUserDTO getCreateUser() {
        return createUser;
    }

    public void setCreateUser(SystemUserDTO createUser) {
        this.createUser = createUser;
    }

    public SystemUserDTO getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(SystemUserDTO lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RouteTypeDTO routeType = (RouteTypeDTO) o;
        return Objects.equals(id, routeType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
