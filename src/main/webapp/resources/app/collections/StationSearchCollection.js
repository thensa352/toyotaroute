define(['jquery', 'underscore', 'backbone', 'app/models/StationModel'], function ($, _, Backbone, Station) {
    var StationSearch = Backbone.Collection.extend({
        model: Station,
        initialize: function (models, options) {
            this.cityName = options.cityName;
            this.routeTypeName = options.routeTypeName;
            this.routeName = options.routeName;
            this.totalDuration = options.totalDuration;
            this.peronNo = options.peronNo;
            this.vehicleType = options.vehicleType;
        },
        url: function () {
            if (typeof this.cityName == 'undefined' || this.cityName == "" || this.cityName == "Şehir Seçiniz" || !isNaN(this.cityName))
                this.cityName = "null";
            if (typeof this.routeTypeName == 'undefined' || this.routeTypeName == "" || this.routeTypeName == "Güzergah Tipi Seçiniz")
                this.routeTypeName = "null";
            if (typeof this.routeName == 'undefined' || this.routeName == "" || this.routeName == "Güzergah Seçiniz")
                this.routeName = "null";
            if (typeof this.totalDuration =='undefined' || this.totalDuration == "")
                this.totalDuration = "null";
            if (typeof this.peronNo == 'undefined' || this.peronNo == "" || isNaN(this.peronNo))
                this.peronNo = 0;
            if (typeof this.vehicleType == 'undefined' || this.vehicleType == "" || isNaN(this.vehicleType))
                this.vehicleType = 0;
            return "rest/station/search/" + this.cityName + "/" + this.routeTypeName + "/" + this.routeName + "/"
                + this.totalDuration + "/" + this.peronNo + "/" + this.vehicleType;
        }
    });
    return StationSearch;
});




