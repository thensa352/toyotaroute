define(['jquery', 'underscore', 'backbone', 'app/models/RouteModel'], function ($, _, Backbone, Route) {
    var RouteSearch = Backbone.Collection.extend({
        model: Route,
        initialize: function (models, options) {
            this.cityName = options.cityName;
            this.routeTypeName = options.routeTypeName;
        },
        url: function () {
            if (typeof this.cityName == 'undefined' || this.cityName == "" || this.cityName == "Şehir Seçiniz")
                this.cityName = "null";
            if (typeof this.routeTypeName == 'undefined' || this.routeTypeName == "" || this.routeTypeName == "Güzergah Tipi Seçiniz")
                this.routeTypeName = "null";
            return "rest/routes/search/" + this.cityName + "/" + this.routeTypeName;
        }
    });
    return RouteSearch;
});


