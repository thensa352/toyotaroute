define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'spin',
    'app/models/LoginModel',
    'app/collections/RouteCollection',
    'app/collections/CityCollection',
    'app/collections/RouteTypeCollection',
    'app/collections/StationCollection',
    'app/views/CityView',
    'app/views/RouteTypeView',
    'app/views/RouteView',
    'app/views/StationView',
    'app/views/HomepageView',
    'app/views/SearchView',
    'app/views/SelectCityView',
    'app/views/SelectRouteTypeView',
    'app/views/SelectRouteView',
    'app/views/StationSelectView',
    'app/views/MenuView',
    'app/views/UserLoginView',
    'app/views/EmptyDataView'
], function ($,
             _,
             Backbone,
             Handlebars,
             Spinner,
             Login,
             Routes,
             Cities,
             RouteTypes,
             Stations,
             CityViews,
             RouteTypeViews,
             RouteViews,
             StationViews,
             HomepageViews,
             SearchViews,
             SelectCityViews,
             SelectRouteTypeViews,
             SelectRouteViews,
             StationSelectViews,
             MenuView,
             UserLoginView,
             EmptyDataView) {

    var Router = Backbone.Router.extend({
        initialize: function () {
            $(window).scroll(function () {//Ekran aşağı kaydırıldığında Yönetici butonunun tablo üzerinde olmaması için
                var winTop = $(window).scrollTop();
                if (winTop >= 20) {
                    $("#systemAdminButton").stop().fadeTo(400, 0);
                    $("#systemAdminButtonSmall").stop().fadeTo(400, 1);
                } else {
                    $("#systemAdminButton").stop().fadeTo(400, 1);
                    $("#systemAdminButtonSmall").stop().fadeTo(400, 0);
                }
            });
        },
        //Tüm routelar tanımlanıyor.
        routes: {
            '': 'home',
            'search': 'search',
            'login': 'login',
            'cities': 'cities',
            'routeTypes': 'routeTypes',
            'routes': 'routesPage',
            'stations': 'stations',
            'menu': 'menu'
        },

        home: function () {
            var spinner = new Spinner();
            $('body').after(spinner.spin().el);

            //Sayfa ilk açıldığında veri giriş alanları ve tablo render ediliyor.
            disposeView(new HomepageViews.AddHomePageView().render());
            //Sayfada taşma varsa butonları yerleri değiştiriliyor.
            changeButtons();

            //Eğer IE 8 ve IE 9 'dan giriliyorsa placeholder çalışmadığı için yazılar gösteriliyor.
            if ((navigator.appVersion.indexOf("MSIE 8") != -1) || (navigator.appVersion.indexOf("MSIE 9") != -1))
                $(".forIE8").show();

            //Selectlerin doldurulması için ilgili viewler oluşturuluyor.
            var routeRouteTypes = new SelectRouteTypeViews.SelectRouteTypeView();
            var routeCities = new SelectCityViews.SelectCityView();
            var routes = new SelectRouteViews.SelectRouteView();
            var stations = new StationSelectViews.SelectStationView();

            //City select dolduruluyor
            SelectCityViews.cities.fetch({
                cache: false,
                success: function (m_cities) {
                    SelectCityViews.cities = m_cities;
                    routeCities.render(SelectCityViews.cities);
                }
            });
            //RouteType select dolduruluyor.
            SelectRouteTypeViews.routeTypes.fetch({
                cache: false,
                success: function (m_routeTypes) {
                    SelectRouteTypeViews.routeTypes = m_routeTypes;
                    routeRouteTypes.render(m_routeTypes);
                }
            });

            //İlk olarak durak ve güzergah seçim alanları boş olarak dolduruluyor.
            routes.render(SelectRouteViews.routes);
            stations.render(StationSelectViews.stations);

            //Tüm duraklar tabloya basılıyor.
            var stations = new Stations();
            stations.fetch({
                cache: false,
                success: function (m_stations) {
                    spinner.stop();
                    //Eğer hiç durak yoksa bu durum için hazırlanmış template ekrana basılıyor.
                    if (m_stations.size() == 0) {
                        var emptyDataView = new EmptyDataView();
                        emptyDataView.model = {column: 9, message: 'Hiç durak eklenmemiş.'};
                        $("#homepageTable").append(emptyDataView.render().el);
                        changeButtons();
                    }
                    else {//Durak varsa hepsi ekrana basılıyor.
                        m_stations.each(function (m_model) {
                            var homepageView = new HomepageViews.HomePageView();
                            homepageView.model = m_model;
                            $("#homepageTable").append(homepageView.render().el);
                        });
                        changeButtons();
                    }
                }
            });
        },

        cities: function () {
            var spinner = new Spinner();
            $('body').after(spinner.spin().el);

            var that = this;
            //Sistem yöneticisi olmayanların girişini engellemek için login nesnesi oluşturuluyor.
            var login = new Login();
            login.fetch({
                cache: false,
                success: function (m_login) {
                    if (m_login.toJSON().username == null)//Kullanıcı girişi yapılmamışsa.
                        that.navigate('login', {trigger: true});//Logine git.
                    else {//Kullanıcı giriş yapmışsa sayfa normal olarak açılıyor.

                        //Veri giriş alanı ve tablo ekrana basılıyor.
                        disposeView(new CityViews.AddCityView().render());

                        //Sayfada taşma varsa butonları yerleri değiştiriliyor.
                        changeButtons();

                        //IE8'de placeholder uyumluluğu bulunmadığından labellar kullanılıyor
                        if ((navigator.appVersion.indexOf("MSIE 8") != -1) || (navigator.appVersion.indexOf("MSIE 9") != -1)) {
                            $(".forIE8").show();
                        }
                        //Sağ üst köşedeki butona giriş yapan yöneticinin adı soyadı basılıyor.
                        $("#systemAdminButton").append(m_login.toJSON().firstName + " " + m_login.toJSON().lastName);

                        var cities = new Cities();

                        cities.fetch({
                            cache: false,
                            success: function (m_cities) {
                                spinner.stop();
                                //Hiç şehir yoksa bu durum için hazırlanmış template ekrana basılıyor.
                                if (m_cities.size() == 0) {
                                    var emptyDataView = new EmptyDataView();
                                    emptyDataView.model = {column: 3, message: 'Hiç şehir eklenmemiş.'};
                                    $("#cityTemplateTable").append(emptyDataView.render().el);
                                }
                                //Şehir varsa hepsi tabloya basılıyor.
                                else {
                                    m_cities.each(function (m_model) {
                                        var cityView = new CityViews.CityView();
                                        cityView.model = m_model;
                                        $("#cityTemplateTable").append(cityView.render().el);

                                    });
                                }
                                changeButtons();
                            }
                        });
                    }
                }
            });
        },

        routeTypes: function () {
            var spinner = new Spinner();
            $('body').after(spinner.spin().el);

            var that = this;
            //Sistem yöneticisi olmayanların girişini engellemek için login nesnesi oluşturuluyor.
            var login = new Login();

            login.fetch({
                cache: false,
                success: function (m_login) {
                    if (m_login.toJSON().username == null)//Kullanıcı girişi yapılmamışsa
                        that.navigate('login', {trigger: true});//Logine git
                    else {
                        //Veri giriş alanları ve tablo ekrana basılıyor.
                        disposeView(new RouteTypeViews.AddRouteTypeView().render());

                        changeButtons();
                        //IE8'de placeholder uyumluluğu bulunmadığından labellar kullanılıyor
                        if ((navigator.appVersion.indexOf("MSIE 8") != -1) || (navigator.appVersion.indexOf("MSIE 9") != -1))
                            $(".forIE8").show();
                        //Sağ üst köşedeki butona giriş yapan yöneticinin adı soyadı basılıyor.
                        $("#systemAdminButton").append(m_login.toJSON().firstName + " " + m_login.toJSON().lastName);

                        var routeTypes = new RouteTypes();
                        routeTypes.fetch({
                            cache: false,
                            success: function (m_routeTypes) {
                                spinner.stop();
                                //Eğer hiç güzergah yoksa bu durum için hazırlanmış template ekrana basılıyor.
                                if (m_routeTypes.size() == 0) {
                                    var emptyDataView = new EmptyDataView();
                                    emptyDataView.model = {column: 3, message: 'Hiç güzergah tipi eklenmemiş.'};
                                    $("#routeTypeTemplateTable").append(emptyDataView.render().el);
                                }
                                //Güzergah varsa hepsi ekrana basılıyor.
                                else {
                                    m_routeTypes.each(function (m_model) {
                                        var routeTypeView = new RouteTypeViews.RouteTypeView();
                                        routeTypeView.model = m_model;
                                        $("#routeTypeTemplateTable").append(routeTypeView.render().el);
                                        changeButtons();
                                    });
                                }
                            }
                        });
                    }
                }
            });
        },

        routesPage: function () {
            var spinner = new Spinner();
            $('body').after(spinner.spin().el);

            var that = this;

            //Sistem yöneticisi olmayanların girişini engellemek için login nesnesi oluşturuluyor.
            var login = new Login();

            login.fetch({
                cache: false,
                success: function (m_login) {
                    if (m_login.toJSON().username == null)//Kullanıcı girişi yapılmamışsa
                        that.navigate('login', {trigger: true});//Logine git
                    else {
                        //Veri giriş alanları ve tablo ekrana basılıyor.
                        disposeView(new RouteViews.AddRouteView().render());

                        changeButtons();
                        //IE8'de placeholder uyumluluğu bulunmadığından labellar kullanılıyor
                        if ((navigator.appVersion.indexOf("MSIE 8") != -1) || (navigator.appVersion.indexOf("MSIE 9") != -1))
                            $(".forIE8").show();
                        //Sağ üst köşedeki butona giriş yapan yöneticinin adı soyadı basılıyor.
                        $("#systemAdminButton").append(m_login.toJSON().firstName + " " + m_login.toJSON().lastName);

                        //Selectlerin içini doldurmak için gerekli viewlar oluşturuluyor.
                        var routeCities = new SelectCityViews.SelectCityView();
                        var routeRouteTypes = new SelectRouteTypeViews.SelectRouteTypeView();

                        //City select dolduruluyor.
                        SelectCityViews.cities.fetch({
                            cache: false,
                            success: function (m_cities) {
                                SelectCityViews.cities = m_cities;
                                routeCities.render(SelectCityViews.cities);
                            }
                        });

                        //RouteType select dolduruluyor.
                        SelectRouteTypeViews.routeTypes.fetch({
                            cache: false,
                            success: function (m_routeTypes) {
                                SelectRouteTypeViews.routeTypes = m_routeTypes;
                                routeRouteTypes.render(m_routeTypes);
                            }
                        });

                        var routes = new Routes();

                        //Tüm güzergahlar çekiliyor.
                        routes.fetch({
                            cache: false,
                            success: function (m_routes) {
                                spinner.stop();
                                //Eğer güzergah yoksa ekrana bu durum için hazırlanmış template basılıyor.
                                if (m_routes.size() == 0) {
                                    var emptyDataView = new EmptyDataView();
                                    emptyDataView.model = {column: 8, message: 'Hiç güzergah eklenmemiş.'};
                                    $("#routeTable").append(emptyDataView.render().el);
                                }
                                //Eğer güzergah varsa hepsi ekrana basılıyor.
                                else {
                                    m_routes.each(function (m_model) {
                                        var routeView = new RouteViews.RouteView();
                                        routeView.model = m_model;
                                        $("#routeTable").append(routeView.render().el);
                                    });
                                }
                                changeButtons();
                            }
                        });
                    }
                }
            });
        },

        stations: function () {
            var spinner = new Spinner();
            $('body').after(spinner.spin().el);

            var that = this;

            //Sistem yöneticisi olmayanların girişini engellemek için login nesnesi oluşturuluyor.
            var login = new Login();

            login.fetch({
                cache: false,
                success: function (m_login) {
                    if (m_login.toJSON().username == null)//Kullanıcı girişi yapılmamışsa
                        that.navigate('login', {trigger: true});//Logine git
                    //Giriş yapılmışsa
                    else {
                        //Veri giriş alanları ve tablo ekrana basılıyor.
                        disposeView(new StationViews.AddStationView().render());

                        changeButtons();
                        //IE8'de placeholder uyumluluğu bulunmadığından labellar kullanılıyor
                        if ((navigator.appVersion.indexOf("MSIE 8") != -1) || (navigator.appVersion.indexOf("MSIE 9") != -1))
                            $(".forIE8").show();
                        //Sağ üst köşedeki butona giriş yapan yöneticinin adı soyadı basılıyor.
                        $("#systemAdminButton").append(m_login.toJSON().firstName + " " + m_login.toJSON().lastName);

                        //Route selecti doldurmak için viewden nesne oluşturuluyor.
                        var stationRoutes = new SelectRouteViews.SelectRouteView();

                        //Route select dolduruluyor.
                        SelectRouteViews.routes.fetch({
                            cache: false,
                            success: function (m_routes) {
                                SelectRouteViews.routes = m_routes;
                                stationRoutes.render(SelectRouteViews.routes);
                            }
                        });

                        var stations = new Stations();

                        //Tüm duraklar çekiliyor.
                        stations.fetch({
                            cache: false,
                            success: function (m_stations) {
                                spinner.stop();
                                //Eğer durak yoksa ekrana bu durum için hazırlanmış template basılıyor.
                                if (m_stations.size() == 0) {
                                    var emptyDataView = new EmptyDataView();
                                    emptyDataView.model = {column: 9, message: 'Hiç durak eklenmemiş.'};
                                    $("#stationListTable").append(emptyDataView.render().el);
                                }
                                //Eğer durak varsa hepsini ekrana bas
                                else {
                                    m_stations.each(function (m_model) {
                                        var stationView = new StationViews.StationView();
                                        stationView.model = m_model;
                                        $("#stationListTable").append(stationView.render().el);
                                    });
                                }
                                changeButtons();
                            }
                        });
                    }
                }
            });
        },

        search: function () {
            var spinner = new Spinner();
            $('body').after(spinner.spin().el);

            //Tablo ekrana basılıyor.
            disposeView(new SearchViews.AddSearchView().render());

            changeButtons();
            var stations = new Stations();
            //Tüm duraklar çekiliyor.
            stations.fetch({
                cache: false,
                success: function (m_stations) {
                    spinner.stop();
                    //Eğer hiç durak yoksa bu durum için hazırlanmış template ekrana basılıyor.
                    if (m_stations.size() == 0) {
                        var emptyDataView = new EmptyDataView();
                        emptyDataView.model = {column: 10, message: 'Hiç durak eklenmemiş.'};
                        $("#searchTable").append(emptyDataView.render().el);
                    }
                    //Eğer durak varsa hepsi ekrana basılıyor.
                    else {
                        m_stations.each(function (m_model) {
                            var searchView = new SearchViews.SearchView();
                            searchView.model = m_model;
                            $("#searchTable").append(searchView.render().el);
                            changeButtons();
                        });
                    }
                }
            });

        },

        login: function () {
            var spinner = new Spinner();
            $('body').after(spinner.spin().el);

            var that = this;
            var login = new Login();

            login.fetch({
                cache: false,
                success: function (m_login) {
                    spinner.stop();
                    if (m_login.toJSON().username != null)//Kullanıcı giriş yapmışsa
                        that.navigate('menu', {trigger: true});//Menuye at
                    //Kullanıcı giriş yapmadıysa
                    else {
                        //Giriş formunu ekrana bas.
                        disposeView(new UserLoginView().render());
                        if ((navigator.appVersion.indexOf("MSIE 8") != -1) || (navigator.appVersion.indexOf("MSIE 9") != -1))
                            $(".forIE8").show();
                        //Eğer hatalı deneme yapılmadıysa hatayı gizle.
                        if (getParameterByName("error") != 1) {
                            $("#userLoginButton").tooltip("destroy");
                        }
                        //Eğer hatalı deneme yapıldıysa hatayı görünür yap.
                        else {
                            $("#userLoginButton").attr("title", "Sistem yöneticisi değilsiniz. IS Kullanıcı Yardım Masasına başvurunuz. Tel:6666")
                            $("#userLoginButton").tooltip("show");
                            setTimeout(function () {
                                $("#userLoginButton").tooltip("destroy");
                            }, 2000);
                            $("#userLoginButton").attr("title", "");
                        }
                    }
                }
            });
        },

        menu: function () {
            var spinner = new Spinner();
            $('body').after(spinner.spin().el);

            var that = this;
            var login = new Login();

            login.fetch({
                cache: false,
                success: function (m_login) {
                    spinner.stop();
                    if (m_login.toJSON().username == null) {//Kullanıcı giriş yapmadıysa.
                        that.navigate('login', {trigger: true});//Logine at.
                    }
                    //Kullanıcı giriş yaptıysa
                    else {
                        //Menuyu ekrana bas.
                        disposeView(new MenuView().render());

                        //Sağ üst köşedeki butona giriş yapan yöneticinin adı soyadı basılıyor.
                        $("#systemAdminButton").append(m_login.toJSON().firstName + " " + m_login.toJSON().lastName);
                    }
                }
            });
        }
    });
    return Router;
});

