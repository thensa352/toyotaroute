define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var RouteType = Backbone.Model.extend({
        urlRoot: "/toyota-route/rest/routeType",
        validate: function (attrs) {
            if (attrs.routeTypeName == "")
                return "Güzergah Tipi Adı Boş Geçilemez!";
        }
    });
    var routeType = new RouteType();
    return {
        RouteType: RouteType,
        routeType: routeType
    };
});


