define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var Login = Backbone.Model.extend({
        urlRoot: "/toyota-route/rest/login"
    });
    return Login;
});

