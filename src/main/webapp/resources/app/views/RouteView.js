define(['jquery',
        'underscore',
        'backbone',
        'handlebars',
        'app/models/RouteModel',
        'app/models/CityModel',
        'app/models/RouteTypeModel',
        'app/collections/RouteSearchCollection',
        'app/views/RouteView',
        'app/views/SelectCityView',
        'app/views/SelectRouteTypeView',
        'app/views/EmptyDataView',
        'text!app/templates/route-list-template.html',
        'text!app/templates/select-routeType-template.html',
        'text!app/templates/select-city-template.html',
        'text!app/templates/route-template.html'],
    function ($,
              _,
              Backbone,
              Handlebars,
              Route,
              City,
              RouteType,
              RouteSearch,
              RouteViews,
              SelectCityView,
              SelectRouteTypeView,
              EmptyDataView,
              RouteListTemplate,
              RouteSelectRouteTypeTemplate,
              RouteSelectCityTemplate,
              RouteTemplate) {
        var RouteView = Backbone.View.extend({
            tagName: "tr",
            model: Route,
            events: {
                'click .editRoute': 'onEditRoute',
                'click .cancelRoute': 'onCancelRoute',
                'click .updateRoute': 'updateRoute',
                'click .deleteRoute': 'deleteRoute',
                'keyup #vehicleType': 'changeField'
            },
            onEditRoute: function () {
                this.$el.addClass("info");
                this.$el.find("select").show();
                this.$el.find("input").show().focus();
                this.$el.find("span").hide();
                $(".editRoute").hide();
                $(".deleteRoute").hide();
                this.$el.find(".updateRoute").show();
                this.$el.find(".cancelRoute").show();
                var routeCities = new SelectCityView.SelectCityView();
                var routeRouteTypes = new SelectRouteTypeView.SelectRouteTypeView();
                routeCities.render(SelectCityView.cities);
                routeRouteTypes.render(SelectRouteTypeView.routeTypes);
                var selectedCityId = this.model.attributes.city.id;
                var selectedRouteTypeId = this.model.attributes.routeType.id;
                this.$el.find(".citySelect option").each(function () {
                    if ($(this).val() == selectedCityId) {
                        $(this).attr("selected", "true");
                    }
                });
                this.$el.find(".routeTypeSelect option").each(function () {
                    if ($(this).val() == selectedRouteTypeId) {
                        $(this).attr("selected", "true");
                    }
                });
            },
            onCancelRoute: function () {
                this.$el.removeClass("info");
                $(".editRoute").show();
                $(".deleteRoute").show();
                this.render();
            },
            updateRoute: function () {
                //Yeni bilgiler modele set edilmeden önce değişkenlere atanıyor..
                var routeName = this.$el.find(".routeNameAdd").val();
                var totalDuration = this.$el.find(".totalDurationAdd").val();
                var peronNo = this.$el.find(".peronNoAdd").val();
                var vehicleType = this.$el.find(".vehicleTypeAdd").val();
                var city = new City({
                    id: this.$el.find(".citySelectAdd").val(),
                    cityName: this.$el.find(".citySelectAdd option:selected").text()
                });
                var routeType = new RouteType.RouteType({
                    id: this.$el.find(".routeTypeSelectAdd").val(),
                    routeTypeName: this.$el.find(".routeTypeSelectAdd option:selected").text()
                });

                var newValues=new Route();
                newValues.set("routeName", routeName);
                newValues.set("totalDuration", totalDuration);
                newValues.set("peronNo", peronNo);
                newValues.set("vehicleType", vehicleType);
                newValues.set("city", city.attributes);
                newValues.set("routeType", routeType.attributes);

                if (!newValues.isValid()) {
                    $("." + newValues.validationError).tooltip("show");
                    setTimeout(function () {
                        $("." + newValues.validationError).tooltip("destroy");
                    }, 1000);
                } else {
                    this.$el.removeClass("info");
                    this.model.set("routeName", routeName);
                    this.model.set("totalDuration", totalDuration);
                    this.model.set("peronNo", peronNo);
                    this.model.set("vehicleType", vehicleType);
                    this.model.set("city", city.attributes);
                    this.model.set("routeType", routeType.attributes);
                    this.model.save();
                    $(".editRoute").show();
                    $(".deleteRoute").show();
                    this.render();
                }
            },
            deleteRoute: function () {
                this.$el.find(".deleteRoute").addClass("disabled");
                var that = this;
                this.model.destroy({
                    wait: true,
                    success: function () {
                        that.remove();
                        if (!$("#routeTable").has(".resultRow").length) {
                            var emptyDataView = new EmptyDataView();
                            emptyDataView.model = {column: 8, message: 'Hiç güzergah eklenmemiş.'};
                            $(".resultRow").parent().empty();
                            $("#routeTable").append(emptyDataView.render().el);
                        }
                        changeButtons();
                    },
                    error: function () {
                        that.$el.find(".deleteRoute").removeClass("disabled");
                        $('.errorModal').modal('toggle');
                    }
                }); // HTTP DELETE
            },
            changeField: function () {
                var that = this;
                if (isNaN(this.$el.find("#vehicleType").val()) ||
                    (parseInt(this.$el.find("#vehicleType").val()) > 3 || parseInt(this.$el.find("#vehicleType").val()) <= 0 )) {
                    this.$el.find("#vehicleType").val("");
                    this.$el.find("#vehicleType").attr("title", "1-Minibüs,2-Midibüs,3-Otobüs");
                    this.$el.find("#vehicleType").tooltip("show");
                    this.$el.find("#vehicleType").attr("title", "Boş geçilemez!");
                }
                setTimeout(function () {
                    that.$el.find("#vehicleTypeAdd").tooltip("destroy");
                }, 1000);
            },
            render: function () {
                var that = this;
                var template = Handlebars.compile(RouteListTemplate);
                var myHtml = template(that.model.toJSON());
                that.$el.html(myHtml);
                return this;
            }
        });

        var AddRouteView = Backbone.View.extend({
            el: $(".page"),
            events: {
                'click .saveRoute': 'saveRoute',
                'change #citySelectAdd': 'selectCityAndRouteType',
                'change #routeTypeSelectAdd': 'selectCityAndRouteType',
                'keyup #vehicleTypeAdd': 'changeField'
            },
            saveRoute: function () {
                var route = new Route();
                route.set("routeName", $("#routeNameAdd").val());
                route.set("totalDuration", $("#totalDurationAdd").val());
                route.set("peronNo", $("#peronNoAdd").val());
                route.set("vehicleType", $("#vehicleTypeAdd").val());
                var city = new City({
                    id: $("#citySelectAdd").val(),
                    cityName: $("#citySelectAdd option:selected").text()
                });
                var routeType = new RouteType.RouteType({
                    id: $("#routeTypeSelectAdd").val(),
                    routeTypeName: $("#routeTypeSelectAdd option:selected").text()
                });
                route.set("city", city.attributes);
                route.set("routeType", routeType.attributes);
                if (!route.isValid()) {
                    $("#" + route.validationError).tooltip("show");
                    setTimeout(function () {
                        $("#" + route.validationError).tooltip("destroy");
                    }, 1000);
                } else {
                    $("input").tooltip("destroy");
                    $("select").tooltip("destroy");
                    route.save();
                    $("#routeNameAdd").val("");
                    $("#totalDurationAdd").val("");
                    $("#peronNoAdd").val("");
                    $("#vehicleTypeAdd").val("");
                    var routeView = new RouteView();
                    routeView.model = route;
                    if ($("#routeTable").has(".emptyRow").length)
                        $(".resultRow").parent().empty();
                    $("#routeTable").append(routeView.render().el);
                    changeButtons();
                }
            },
            changeField: function () {
                if (isNaN($("#vehicleTypeAdd").val()) ||
                    (parseInt($("#vehicleTypeAdd").val()) > 3 || parseInt($("#vehicleTypeAdd").val()) <= 0 )) {
                    $("#vehicleTypeAdd").val("");
                    $("#vehicleTypeAdd").attr("title", "1-Minibüs,2-Midibüs,3-Otobüs");
                    $("#vehicleTypeAdd").tooltip("show");
                    $("#vehicleTypeAdd").attr("title", "Boş geçilemez!");
                }
                setTimeout(function () {
                    $("#vehicleTypeAdd").tooltip("destroy");
                }, 1000);
            },
            selectCityAndRouteType: function () {
                var citySelected = $("#citySelectAdd option:selected").text();
                var routeTypeSelected = $("#routeTypeSelectAdd option:selected").text();
                var searchRoutes = new RouteSearch([], {cityName: citySelected, routeTypeName: routeTypeSelected});
                searchRoutes.fetch({//Tüm güzergahları getir.
                    cache: false,//for IE
                    success: function (m_routes) {
                        $(".resultRow").parent().empty();
                        if (m_routes.size() == 0) {
                            var emptyDataView = new EmptyDataView();
                            emptyDataView.model = {column: 8, message: 'Hiç güzergah eklenmemiş.'};
                            $(".resultRow").parent().empty();
                            $("#routeTable").append(emptyDataView.render().el);
                        } else {
                            m_routes.each(function (m_model) {
                                var routeView = new RouteView();
                                routeView.model = m_model;
                                $("#routeTable").append(routeView.render().el);
                            });
                        }
                        changeButtons();
                    }
                });
            },
            render: function () {
                this.$el.html(RouteTemplate);
                return this;
            }
        });

        return {
            AddRouteView: AddRouteView,
            RouteView: RouteView
        };
    });


