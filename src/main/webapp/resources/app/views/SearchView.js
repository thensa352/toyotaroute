define(['jquery',
        'underscore',
        'backbone',
        'handlebars',
        'spin',
        'app/models/RouteModel',
        'app/models/StationModel',
        'app/collections/RouteCollection',
        'app/collections/SearchCollection',
        'app/views/SelectRouteView',
        'app/views/StationSelectView',
        'app/views/StationView',
        'app/views/EmptyDataView',
        'text!app/templates/search-list-template.html',
        'text!app/templates/search-template.html'],
    function ($,
              _,
              Backbone,
              Handlebars,
              Spinner,
              Route,
              Station,
              Routes,
              Searches,
              SelectRouteViews,
              StationSelectViews,
              StationViews,
              EmptyDataView,
              SearchListTemplate,
              SearchTemplate) {

        var SearchView = Backbone.View.extend({
            tagName: "tr",
            model: Station,
            render: function () {
                var that = this;
                var template = Handlebars.compile(SearchListTemplate);
                var myHtml = template(that.model.toJSON());
                that.$el.html(myHtml);
                return this;
            }
        });

        var AddSearchView = Backbone.View.extend({
            el: $(".page"),
            events: {
                'keyup #cityNameSearch': 'Search',
                'keyup #routeTypeNameSearch': 'Search',
                'keyup #routeNameSearch': 'Search',
                'keyup #totalDurationSearch': 'Search',
                'keyup #peronNoSearch': 'Search',
                'keyup #vehicleTypeSearch': 'Search',
                'keyup #stationNameSearch': 'Search',
                'keyup #stationNoSearch': 'Search',
                'keyup #departureTimeSearch': 'Search',
                'keyup #arrivalTimeSearch': 'Search',
                'focus #vehicleTypeSearch': 'onVehicleType'
            },
            onVehicleType: function () {
                $("#vehicleTypeSearch").tooltip("show");
                setTimeout(function () {
                    $("#vehicleTypeSearch").tooltip("destroy");
                }, 1000);
            },
            Search: function () {
                var spinner=new Spinner();
                $('#searchTable').after(spinner.spin().el);
                //Tüm inputlardaki değerler alınıyor..
                var cityName = $("#cityNameSearch").val();
                var routeTypeName = $("#routeTypeNameSearch").val();
                var routeName = $("#routeNameSearch").val();
                var totalDuration = $("#totalDurationSearch").val();
                var peronNo = $("#peronNoSearch").val();
                var vehicleType = $("#vehicleTypeSearch").val();
                var stationName = $("#stationNameSearch").val();
                var stationNo = $("#stationNoSearch").val();
                var departureTime = $("#departureTimeSearch").val();
                var arrivalTime = $("#arrivalTimeSearch").val();

                //Farklı tipte karakter girilmesi engelleniyor.
                if (isNaN(vehicleType))
                    $("#vehicleTypeSearch").val("");

                //Inputlardan alınan parametreler arama yapmak için atanıyor..
                var search = new Searches([], {
                    cityName: cityName,
                    routeTypeName: routeTypeName,
                    routeName: routeName,
                    totalDuration: totalDuration,
                    peronNo: peronNo,
                    vehicleType: vehicleType,
                    stationName: stationName,
                    stationNo: stationNo,
                    departureTime: departureTime,
                    arrivalTime: arrivalTime
                });

                //Atanan değerlere göre arama yapılıp sonuçlar tabloya ekleniyor.
                search.fetch({
                    cache: false,
                    success: function (m_stations) {
                        spinner.stop();
                        $(".resultRow").parent().empty();
                        if (m_stations.size() == 0) {
                            var emptyDataView = new EmptyDataView();
                            emptyDataView.model = {column: 10, message: 'Arama kriterlerine göre kayıt bulunamadı.'};
                            $("#searchTable ").append(emptyDataView.render().el);
                        } else {
                            m_stations.each(function (m_station) {
                                var searchView = new SearchView();
                                searchView.model = m_station;
                                $("#searchTable").append(searchView.render().el);
                            });
                        }
                        changeButtons();
                    }
                });
            },
            render: function () {
                this.$el.html(SearchTemplate);
                return this;
            }
        });

        return {
            SearchView: SearchView,
            AddSearchView: AddSearchView
        };
    });


