define(['jquery',
        'underscore',
        'backbone',
        'handlebars',
        'app/models/RouteModel',
        'app/models/StationModel',
        'app/collections/StationCollection',
        'app/collections/SearchCollection',
        'app/views/SelectRouteView',
        'app/views/EmptyDataView',
        'text!app/templates/station-list-template.html',
        'text!app/templates/select-route-template.html',
        'text!app/templates/station-template.html'],
    function ($,
              _,
              Backbone,
              Handlebars,
              Route,
              Station,
              Stations,
              Search,
              SelectRouteViews,
              EmptyDataView,
              StationListTemplate,
              StationSelectRouteTemplate,
              StationTemplate) {

        var StationView = Backbone.View.extend({
            tagName: "tr",
            model: Station,
            events: {
                'click .onEditStation': 'onEditStation',
                'click .cancelStation': 'onCancelStation',
                'click .updateStation': 'updateStation',
                'click .deleteStation': 'deleteStation',
                'change .routeSelect': 'routeSelect'
            },
            onEditStation: function () {
                this.$el.addClass("info");
                this.$el.find("select").show();
                this.$el.find("input").show();
                this.$el.find("span").hide();
                $(".deleteStation").hide();
                $(".onEditStation").hide();
                this.$el.find(".updateStation").show();
                this.$el.find(".cancelStation").show();
                var stationRoutes = new SelectRouteViews.SelectRouteView();
                stationRoutes.render(SelectRouteViews.routes);
                var selectedRouteId = this.model.attributes.route.id;
                this.$el.find(".routeSelect option").each(function () {
                    if ($(this).val() == selectedRouteId) {
                        $(this).attr("selected", "true");
                    }
                });
            },
            onCancelStation: function () {
                this.$el.removeClass("info");
                $(".onEditStation").show();
                $(".deleteStation").show();
                this.render();
            },
            updateStation: function () {
                var stationName = this.$el.find(".stationNameAdd").val();
                var stationNo = this.$el.find(".stationNoAdd").val();
                var arrivalTime = this.$el.find(".arrivalTimeAdd").val();
                var departureTime = this.$el.find(".departureTimeAdd").val();

                var route = new Route({
                    id: this.$el.find(".routeSelectStationPage").val(),
                    routeName: this.$el.find(".routeSelectStationPage option:selected").text()
                });
                //Güzergah değiştirildiğinde şehir ve güzergah tipi alanlarını yenilemek için listeyi dolaş.
                SelectRouteViews.routes.each(function (m_model) {
                    if (m_model.id == route.id)
                        route = m_model;
                });
                var that = this;

                var newValues = new Station();
                newValues.set("stationName", stationName);
                newValues.set("stationNo", stationNo);
                newValues.set("arrivalTime", arrivalTime);
                newValues.set("departureTime", departureTime);
                newValues.set("route", route.attributes);

                if (!newValues.isValid()) {
                    $("." + newValues.validationError).tooltip("show");
                    setTimeout(function () {
                        $("." + newValues.validationError).tooltip("destroy");
                    }, 1000);
                } else {
                    this.$el.removeClass("info");
                    this.model.set("stationName", stationName);
                    this.model.set("stationNo", stationNo);
                    this.model.set("arrivalTime", arrivalTime);
                    this.model.set("departureTime", departureTime);
                    this.model.set("route", route.attributes);
                    this.model.save();
                    $(".onEditStation").show();
                    $(".deleteStation").show();
                    this.render();
                }
            },
            deleteStation: function () {
                this.model.destroy();
                this.remove();
                if (!$("#stationListTable").has(".resultRow").length) {
                    var emptyDataView = new EmptyDataView();
                    emptyDataView.model = {column: 9, message: 'Hiç durak eklenmemiş.'};
                    $(".resultRow").parent().empty();
                    $("#stationListTable").append(emptyDataView.render().el);
                }
                changeButtons();
            },
            routeSelect: function () {
                var route = new Route({
                    id: this.$el.find(".routeSelect").val(),
                    routeName: this.$el.find(".routeSelect option:selected").text()
                });
                //Güzergah değiştirildiğinde şehir ve güzergah tipi alanlarını yenilemek için listeyi dolaş.
                SelectRouteViews.routes.each(function (m_model) {
                    if (m_model.id == route.id)
                        route = m_model;
                });
                if (route.id != "") {
                    this.$el.find(".cityName").html(route.attributes.city.cityName);
                    this.$el.find(".routeTypeName").html(route.attributes.routeType.routeTypeName);
                } //Kullanıcı düzenleme aşamasında güzergah seçmeden bırakırsa şehir ve güzergah tipi alanlarını boşalt
                else {
                    this.$el.find(".cityName").html("");
                    this.$el.find(".routeTypeName").html("");
                }
            },
            render: function () {
                var that = this;
                var template = Handlebars.compile(StationListTemplate);
                var myHtml = template(that.model.toJSON());
                that.$el.html(myHtml);
                return this;
            }
        });


        var AddStationView = Backbone.View.extend({
            el: $(".page"),
            events: {
                'click .saveStation': 'saveStation',
                'change #routeSelectStationPage': 'routeSelect'
            },
            saveStation: function () {
                var station = new Station();
                station.set("stationName", $("#stationNameAdd").val());
                station.set("stationNo", $("#stationNoAdd").val());
                station.set("arrivalTime", $("#arrivalTimeAdd").val());
                station.set("departureTime", $("#departureTimeAdd").val());
                var route = new Route({
                    id: $("#routeSelectStationPage").val(),
                    routeName: $("#routeSelectStationPage option:selected").text()
                });
                SelectRouteViews.routes.each(function (m_model) {
                    if (m_model.id == route.id)
                        route = m_model;
                });
                station.set("route", route.attributes);

                if (!station.isValid()) {
                    $("#" + station.validationError).tooltip("show");
                    setTimeout(function () {
                        $("#" + station.validationError).tooltip("destroy");
                    }, 1000);
                } else {
                    $("input").tooltip("destroy");
                    $("select").tooltip("destroy");
                    station.save();
                    $("#stationNameAdd").val("");
                    $("#stationNoAdd").val("");
                    $("#arrivalTimeAdd").val("");
                    $("#departureTimeAdd").val("");
                    var stationView = new StationView();
                    stationView.model = station;
                    if ($("#stationListTable").has(".emptyRow").length)
                        $(".resultRow").parent().empty();
                    $("#stationListTable").append(stationView.render().el);
                    changeButtons();
                }
            },
            routeSelect: function () {
                var routeSelected = $("#routeSelectStationPage option:selected").text();
                var search = new Search([], {
                    routeName: routeSelected
                });

                search.fetch({
                    cache: false,
                    success: function (m_stations) {
                        $(".resultRow").parent().empty();
                        if (m_stations.size() == 0) {
                            var emptyDataView = new EmptyDataView();
                            emptyDataView.model = {column: 9, message: 'Hiç durak eklenmemiş.'};
                            $("#stationListTable").append(emptyDataView.render().el);
                        }
                        else {
                            m_stations.each(function (m_station) {
                                var stationView = new StationView();
                                stationView.model = m_station
                                $("#stationListTable").append(stationView.render().el);
                            });
                        }
                        changeButtons();
                    }
                });
            },
            render: function () {
                this.$el.html(StationTemplate);
                return this;
            }
        });

        return {
            StationView: StationView,
            AddStationView: AddStationView
        };
    });