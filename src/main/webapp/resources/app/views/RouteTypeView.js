define(['jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/RouteTypeModel',
    'app/views/EmptyDataView',
    'text!app/templates/routeType-template.html',
    'text!app/templates/routeType-list-template.html'
], function ($, _, Backbone, Handlebars, RouteType, EmptyDataView, RouteTypeTemplate, RouteTypeListTemplate) {

    var RouteTypeView = Backbone.View.extend({
        tagName: "tr",
        model: RouteType.RouteType,
        events: {
            'click .editRouteType': 'onEditRouteType',
            'click .cancelRouteType': 'onCancelRouteType',
            'click .updateRouteType': 'updateRouteType',
            'click .deleteRouteType': 'deleteRouteType'
        },
        onEditRouteType: function () {
            this.$el.addClass("info");
            this.$el.find("input").show().focus();
            this.$el.find("span").hide();
            $(".editRouteType").hide();
            $(".deleteRouteType").hide();
            this.$el.find(".updateRouteType").show();
            this.$el.find(".cancelRouteType").show();
        },
        onCancelRouteType: function () {
            this.$el.removeClass("info");
            $(".editRouteType").show();
            $(".deleteRouteType").show();
            this.render();
        },
        updateRouteType: function () {
            var routeTypeName = this.$el.find("input").val();
            this.$el.removeClass("info");
            if (!routeTypeName) {
                this.$el.find("input").tooltip("show");
                var that = this;
                setTimeout(function () {
                    that.$el.find("input").tooltip("destroy");
                }, 1000);
            } else {
                this.model.set("routeTypeName", routeTypeName);
                this.model.save();
                this.render();
                $(".editRouteType").show();
                $(".deleteRouteType").show();
                this.render();
            }
        },
        deleteRouteType: function () {
            this.$el.find(".deleteRouteType").addClass("disabled");
            var that = this;
            this.model.destroy({
                wait: true,
                success: function () {
                    that.remove();
                    if (!$("#routeTypeTemplateTable").has(".resultRow").length) {
                        var emptyDataView = new EmptyDataView();
                        emptyDataView.model = {column: 3, message: 'Hiç güzergah tipi eklenmemiş.'};
                        $(".resultRow").parent().empty();
                        $("#routeTypeTemplateTable").append(emptyDataView.render().el);
                    }
                    changeButtons();
                },
                error: function () {
                    that.$el.find(".deleteRouteType").removeClass("disabled");
                    $('.errorModal').modal('toggle');
                }
            }); // HTTP DELETE
        },
        render: function () {
            var that = this;
            var template = Handlebars.compile(RouteTypeListTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;
        }
    });

    var AddRouteTypeView = Backbone.View.extend({
        el: $(".page"),
        events: {
            'click .saveRouteType': 'saveRouteType'
        },
        saveRouteType: function () {
            var routeType = new RouteType.RouteType();
            routeType.set("routeTypeName", $("#routeTypeName").val());
            if (!routeType.isValid()) {
                $("#routeTypeName").tooltip("show");
                setTimeout(function () {
                    $("#routeTypeName").tooltip("destroy");
                }, 1000);
            } else {
                $("#routeTypeName").tooltip("destroy");
                routeType.save();
                $("#routeTypeName").val("");
                var routeTypeView = new RouteTypeView();
                routeTypeView.model = routeType;
                if ($("#routeTypeTemplateTable").has(".emptyRow").length)
                    $(".resultRow").parent().empty();
                $("#routeTypeTemplateTable").append(routeTypeView.render().el);
            }
            changeButtons();
        },
        render: function () {
            this.$el.html(RouteTypeTemplate);
            return this;
        }
    });

    return {
        RouteTypeView: RouteTypeView,
        AddRouteTypeView: AddRouteTypeView
    };
});






