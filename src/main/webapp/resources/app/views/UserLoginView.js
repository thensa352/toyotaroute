define(['jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/user-login-template.html'], function ($, _, Backbone, Handlebars,UserLoginTemplate) {

    var UserLoginView = Backbone.View.extend({
        el: $(".page"),
        render: function () {
            this.$el.html(UserLoginTemplate);
            return this;
        }
    });
    return UserLoginView;
});