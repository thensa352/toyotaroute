define(['jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/CityModel',
    'app/views/EmptyDataView',
    'text!app/templates/city-list-template.html',
    'text!app/templates/city-template.html'], function ($, _, Backbone, Handlebars, City, EmptyDataView, CityListTemplate, CityTemplate) {

    var CityView = Backbone.View.extend({
        tagName: "tr",
        model: City,
        events: {
            'click .editCity': 'onEditCity',
            'click .cancelCity': 'onCancelCity',
            'click .updateCity': 'updateCity',
            'click .deleteCity': 'deleteCity'
        },
        onEditCity: function () {
            this.$el.addClass("info");//Düzenlenecek satıra mavi renk veriliyor.
            this.$el.find("input").show().focus();//Düzenleme modunda imleç inputa getiriliyor.
            this.$el.find(".updateCity").show();//update butonunu göster.
            this.$el.find(".cancelCity").show();//cancel butonunu göster.
            this.$el.find("span").hide();//spanları gizle.
            $(".editCity").hide();//Edit butonunu gizle
            $(".deleteCity").hide();//Delete butonunu gizle
        },
        onCancelCity: function () {
            this.$el.removeClass("info");//Düzenlenecek satıra mavi renk veriliyor.
            $(".editCity").show();//Edit butonunu gizle
            $(".deleteCity").show();//Delete butonunu gizle
            this.render();//İptale basıldığında satırı eski haline getir.
        },
        updateCity: function () {
            var cityName = this.$el.find("input").val();
            if (!cityName) {//İnput boş bırakılırsa
                this.$el.find("input").tooltip("show");//Hata mesajını göster
                var that = this;
                setTimeout(function () {//1 saniye sonra hata mesajını yok et.
                    that.$el.find("input").tooltip("destroy");
                }, 1000);
            }
            else { //İnput dolu ise
                this.$el.removeClass("info");
                this.model.set("cityName", cityName);
                this.model.save();//PUT
                $(".editCity").show();
                $(".deleteCity").show();
                this.render();//Yeni bilgiler kaydedildikten sonra satırı güncelle
            }
        },
        deleteCity: function () {
            this.$el.find(".deleteCity").addClass("disabled");
            var that = this;
            this.model.destroy({
                wait: true,//Yeni değerler set edilmeden önce serverın cevabını bekle. Hata mesajı için gerekli.
                success: function () {
                    that.remove(); //Sorun yoksa modeli sil.

                    //Tüm kayıtlar silinirse
                    if (!$("#cityTemplateTable").has(".resultRow").length) {
                        //Hiç kayıt olmadığı durum için hazırlanan template i ekrana bas.
                        var emptyDataView = new EmptyDataView();
                        emptyDataView.model = {column: 3, message: 'Hiç şehir eklenmemiş.'};
                        $(".resultRow").parent().empty();
                        $("#cityTemplateTable").append(emptyDataView.render().el);
                    }
                    changeButtons();
                },
                //Model silinirken sorun oluşursa hata mesajını görünür yap.
                error: function () {
                    that.$el.find(".deleteCity").removeClass("disabled");
                    $('.errorModal').modal('toggle');
                }
            });
        },
        render: function () {
            var that = this;
            var template = Handlebars.compile(CityListTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;
        }
    });

    var AddCityView = Backbone.View.extend({
        el: $(".page"),
        events: {
            'click #saveCity': 'saveCity'
        },
        saveCity: function () {
            //Kaydedilmek üzere model oluşturuluyor.
            var city = new City();
            city.set("cityName", $("#cityName").val());

            //Girilen şehir adı kontrol ediliyor.
            if (!city.isValid()) {//Hata varsa
                $("#cityName").tooltip("show");
                setTimeout(function () {
                    $("#cityName").tooltip("destroy");
                }, 1000);
            } else {
                //Hata yoksa hata mesajını yok et.
                $("#cityName").tooltip("destroy");

                //Şehri kaydet
                city.save();
                //inputu boşalt.
                $("#cityName").val("");

                var cityView = new CityView();
                cityView.model = city;
                if ($("#cityTemplateTable").has(".emptyRow").length)
                    $(".resultRow").parent().empty();
                $("#cityTemplateTable").append(cityView.render().el);
                changeButtons();//Tablodaki kayıtların ekrandan taşması durumunda butonları değiştirir.
            }
        },
        render: function () {
            this.$el.html(CityTemplate);
            return this;
        }
    });

    return {
        AddCityView: AddCityView,
        CityView: CityView
    };
});