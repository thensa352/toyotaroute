define(['jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/menu-template.html'], function ($, _, Backbone, Handlebars, MenuTemplate) {

    var MenuView = Backbone.View.extend({
        el: $(".page"),
        render: function () {
            this.$el.html(MenuTemplate);
            return this;
        }
    });

    return MenuView;
});