define(['jquery',
        'underscore',
        'backbone',
        'handlebars',
        'spin',
        'app/models/RouteModel',
        'app/models/StationModel',
        'app/collections/RouteCollection',
        'app/collections/StationCollection',
        'app/collections/RouteSearchCollection',
        'app/collections/StationSearchCollection',
        'app/collections/SearchCollection',
        'app/views/SelectRouteView',
        'app/views/StationSelectView',
        'app/views/StationView',
        'app/views/SearchView',
        'app/views/EmptyDataView',
        'text!app/templates/homepage-list.html',
        'text!app/templates/homepage-template.html'],
    function ($,
              _,
              Backbone,
              Handlebars,
              Spinner,
              Route,
              Station,
              Routes,
              Stations,
              RouteSearch,
              StationSearch,
              Searches,
              SelectRouteViews,
              StationSelectViews,
              StationViews,
              SearchViews,
              EmptyDataView,
              HomepageListTemplate,
              HomepageTemplate) {

        var HomePageView = Backbone.View.extend({
            tagName: "tr",
            model: Route,
            render: function () {
                var that = this;
                var template = Handlebars.compile(HomepageListTemplate);
                var myHtml = template(that.model.toJSON());
                that.$el.html(myHtml);
                return this;
            }
        });

        var spinner = new Spinner();

        var AddHomePageView = Backbone.View.extend({
                el: $(".page"),
                events: {
                    'change #citySelectAddHome': 'SelectCityOrRouteType',
                    'change #routeTypeSelectAddHome': 'SelectCityOrRouteType',
                    'change #routeSelectAdd, change #citySelectAddHome, change #routeTypeSelectAddHome': 'SelectRoute',
                    'change #stationSelectAdd': 'SelectStation',
                    'keyup #totalDurationHomepage': 'SearchByAll',
                    'keyup #peronNoHomepage': 'SearchByAll',
                    'keyup #vehicleTypeHomepage': 'SearchByAll'
                },
                SelectCityOrRouteType: function () {//Şehir veya güzergah tipi seçilme durumunda çalışır
                    $('#homepageTable').after(spinner.spin().el);
                    var citySelected = $("#citySelectAddHome option:selected").text();
                    var routeTypeSelected = $("#routeTypeSelectAddHome option:selected").text();
                    var routeSelected = $("#routeSelectAdd option:selected").text();
                    var totalDurationAdd = $("#totalDurationHomepage").val();
                    var peronNoAdd = $("#peronNoHomepage").val();
                    var vehicleTypeAdd = $("#vehicleTypeHomepage").val();
                    var stationRoutes = new SelectRouteViews.SelectRouteView();
                    var stationView = new StationSelectViews.SelectStationView();

                    //Eğer güzergah seçilmiş ve durak selecti doluysa, durak selectini boşalt
                    if (routeSelected != "Güzergah Seçiniz" && StationSelectViews.stations != null) {
                        StationSelectViews.stations = new Stations();
                        stationView.render(StationSelectViews.stations);
                    }

                    //Şehir seçilmemiş ve güzergah listesi doluysa listeyi boşalt
                    if ((citySelected == "Şehir Seçiniz" || routeTypeSelected == "Güzergah Tipi Seçiniz")
                        && SelectRouteViews.routes != null) {
                        SelectRouteViews.routes = new Routes();
                        stationRoutes.render(SelectRouteViews.routes);
                    } else {
                        var searchRoutes = new RouteSearch([], {
                            cityName: citySelected,
                            routeTypeName: routeTypeSelected
                        });
                        searchRoutes.fetch({//Güzergah selectinin içerisi dolduruluyor
                            cache: false,
                            success: function (m_routes) {
                                stationRoutes.render(m_routes);
                            }
                        });
                    }

                    var search = new StationSearch([], {
                        cityName: citySelected,
                        routeTypeName: routeTypeSelected,
                        totalDuration: totalDurationAdd,
                        peronNo: peronNoAdd,
                        vehicleType: vehicleTypeAdd
                    });

                    search.fetch({
                        cache: false,

                        success: function (m_stations) {
                            spinner.stop();
                            $(".resultRow").parent().empty();
                            if (m_stations.size() == 0) {//Eğer hiç kayıt bulunamadıysa
                                var emptyDataView = new EmptyDataView();
                                emptyDataView.model = {
                                    column: 4,
                                    message: 'Arama kriterlerine göre kayıt bulunamadı.'
                                };
                                $("#homepageTable").append(emptyDataView.render().el);
                            }
                            else {//Arama sonuçları ekrana basılıyor
                                m_stations.each(function (m_station) {
                                    var homePageView = new HomePageView();
                                    homePageView.model = m_station;
                                    $("#homepageTable").append(homePageView.render().el);
                                });
                            }
                            changeButtons();
                        }
                    });
                },
                SelectRoute: function () {//Güzergah seçildiğinde çalışır
                    $('#homepageTable').after(spinner.spin().el);
                    var routeSelected = $("#routeSelectAdd option:selected").text();
                    var citySelected = $("#citySelectAddHome option:selected").text();//Seçili olan şehri al
                    var routeTypeSelected = $("#routeTypeSelectAddHome option:selected").text();
                    var totalDurationAdd = $("#totalDurationHomepage").val();
                    var peronNoAdd = $("#peronNoHomepage").val();
                    var vehicleTypeAdd = $("#vehicleTypeHomepage").val();
                    var stationView = new StationSelectViews.SelectStationView();
                    if (routeSelected != "Güzergah Seçiniz" && StationSelectViews.stations != null) {//Güzergah seçildiyse durak selecti doldurulur
                        var stationSearch = new StationSearch([], {
                            cityName: citySelected,
                            routeTypeName: routeTypeSelected,
                            routeName: routeSelected,
                            totalDuration: totalDurationAdd,
                            peronNo: peronNoAdd,
                            vehicleType: vehicleTypeAdd
                        });
                        stationSearch.fetch({
                            cache: false,//for IE
                            success: function (m_stations) {
                                spinner.stop();
                                stationView.render(m_stations);//Sonucu dropdown list'e gönder.
                                $(".resultRow").parent().empty();
                                if (m_stations.size() == 0) {
                                    var emptyDataView = new EmptyDataView();
                                    emptyDataView.model = {
                                        column: 4,
                                        message: 'Arama kriterlerine göre kayıt bulunamadı.'
                                    };
                                    $("#homepageTable").append(emptyDataView.render().el);
                                }
                                else {
                                    m_stations.each(function (m_station) {
                                        var homePageView = new HomePageView();
                                        homePageView.model = m_station;
                                        $("#homepageTable").append(homePageView.render().el);
                                    });
                                }
                                changeButtons();
                            }
                        });
                    } else {//Güzergah seçilmediyse durak selecti boşaltılır
                        StationSelectViews.stations = new Stations();
                        stationView.render(StationSelectViews.stations);

                        var search = new StationSearch([], {
                            cityName: citySelected,
                            routeTypeName: routeTypeSelected,
                            totalDuration: totalDurationAdd,
                            peronNo: peronNoAdd,
                            vehicleType: vehicleTypeAdd
                        });

                        search.fetch({
                            cache: false,
                            success: function (m_stations) {
                                spinner.stop();
                                $(".resultRow").parent().empty();
                                if (m_stations.size() == 0) {
                                    var emptyDataView = new EmptyDataView();
                                    emptyDataView.model = {
                                        column: 4,
                                        message: 'Arama kriterlerine göre kayıt bulunamadı.'
                                    };
                                    $("#homepageTable").append(emptyDataView.render().el)
                                }
                                else {
                                    m_stations.each(function (m_station) {
                                        var searchView = new HomePageView();
                                        searchView.model = m_station;
                                        $("#homepageTable").append(searchView.render().el);
                                    });
                                }
                                changeButtons();
                            }
                        });
                    }
                },
                SelectStation: function () {//Durak seçildiğinde çalışır
                    $('#homepageTable').after(spinner.spin().el);
                    var routeSelected = $("#routeSelectAdd option:selected").text();
                    var stationSelected = $("#stationSelectAdd option:selected").text();
                    var citySelected = $("#citySelectAddHome option:selected").text();//Seçili olan şehri al
                    var routeTypeSelected = $("#routeTypeSelectAddHome option:selected").text();
                    var totalDurationAdd = $("#totalDurationHomepage").val();
                    var peronNoAdd = $("#peronNoHomepage").val();
                    var vehicleTypeAdd = $("#vehicleTypeHomepage").val();
                    var search = new Searches([], {
                        stationName: stationSelected,
                        routeName: routeSelected,
                        cityName: citySelected,
                        routeTypeName: routeTypeSelected,
                        totalDuration: totalDurationAdd,
                        peronNo: peronNoAdd,
                        vehicleType: vehicleTypeAdd
                    });

                    search.fetch({
                        cache: false,
                        success: function (m_stations) {
                            spinner.stop();
                            $(".resultRow").parent().empty();
                            if (m_stations.size() == 0) {
                                var emptyDataView = new EmptyDataView();
                                emptyDataView.model = {
                                    column: 4,
                                    message: 'Arama kriterlerine göre kayıt bulunamadı.'
                                };
                                $("#homepageTable").append(emptyDataView.render().el);
                            }
                            else {
                                m_stations.each(function (m_station) {
                                    var searchView = new HomePageView();
                                    searchView.model = m_station;
                                    $("#homepageTable").append(searchView.render().el);
                                });
                            }
                            changeButtons();
                        }
                    });
                },
                SearchByAll: function () {//Toplam süre, Peron no veya Servis aracına göre arama yapıldığında çalışır
                    $('#homepageTable').after(spinner.spin().el);
                    var that = this;
                    if (isNaN(this.$el.find("#vehicleTypeHomepage").val()) ||
                        (parseInt(this.$el.find("#vehicleTypeHomepage").val()) > 3 || parseInt(this.$el.find("#vehicleTypeHomepage").val()) <= 0 )) {
                        this.$el.find("#vehicleTypeHomepage").val("");
                        this.$el.find("#vehicleTypeHomepage").attr("title", "1-Minibüs,2-Midibüs,3-Otobüs");
                        this.$el.find("#vehicleTypeHomepage").tooltip("show");
                        this.$el.find("#vehicleTypeHomepage").attr("title", "Boş geçilemez!");
                    }
                    setTimeout(function () {
                        that.$el.find("#vehicleTypeHomepage").tooltip("destroy");
                    }, 1000);
                    var totalDurationAdd = $("#totalDurationHomepage").val();
                    var peronNoAdd = $("#peronNoHomepage").val();
                    var vehicleTypeAdd = $("#vehicleTypeHomepage").val();
                    var routeSelected = $("#routeSelectAdd option:selected").text();
                    var stationSelected = $("#stationSelectAdd option:selected").text();
                    var citySelected = $("#citySelectAddHome option:selected").text();//Seçili olan şehri al
                    var routeTypeSelected = $("#routeTypeSelectAddHome option:selected").text();

                    var search = new Searches([], {
                        stationName: stationSelected,
                        routeName: routeSelected,
                        cityName: citySelected,
                        routeTypeName: routeTypeSelected,
                        totalDuration: totalDurationAdd,
                        peronNo: peronNoAdd,
                        vehicleType: vehicleTypeAdd
                    });

                    search.fetch({
                        cache: false,
                        success: function (m_stations) {
                            spinner.stop();
                            $(".resultRow").parent().empty();
                            if (m_stations.size() == 0) {
                                var emptyDataView = new EmptyDataView();
                                emptyDataView.model = {
                                    column: 4,
                                    message: 'Arama kriterlerine göre kayıt bulunamadı.'
                                };
                                $("#homepageTable").append(emptyDataView.render().el);
                            }
                            else {
                                m_stations.each(function (m_station) {
                                    var searchView = new HomePageView();
                                    searchView.model = m_station;
                                    $("#homepageTable").append(searchView.render().el);
                                });
                            }
                            changeButtons();
                        }
                    });
                },
                render: function () {
                    this.$el.html(HomepageTemplate);
                    return this;
                }
            })
            ;

        return {
            HomePageView: HomePageView,
            AddHomePageView: AddHomePageView
        };
    })
;