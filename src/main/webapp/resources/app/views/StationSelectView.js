define(['jquery', 'underscore', 'backbone', 'handlebars',
    'app/collections/StationCollection',
    'app/models/StationModel',
    'text!app/templates/select-station-template.html'], function ($, _, Backbone, Handlebars, Stations, Station,SelectStationTemplate) {

    var stations = new Stations();
    var SelectStationView = Backbone.View.extend({
        model: Station,
        render: function (stations) {
            var template = Handlebars.compile(SelectStationTemplate);
            var myHtml = template({stations: stations.toJSON()});
            $("#stationSelectAdd").html(myHtml);
            $(".stationSelect").html(myHtml);
        }
    });

    return {
        stations:stations,
        SelectStationView:SelectStationView
    };
});




