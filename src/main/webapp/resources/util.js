//Girilen değer numara mı değil mi? Kontrol ediliyor. Numara ise gerekli hata mesajları yazdırılıyor.
function isNumber(evt, el) {
    var element = el;
    if (el.indexOf('.') != 0)//Eğer id değil de sınıf ile input için çalışıyorsa
        element = "#" + el;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) || charCode == 127 || charCode == 8 || charCode == 13)
        return true;
    else {
        $(element).attr("title", "Sadece harf giriniz!");
        $(element).tooltip("show");
        $(element).attr("title", "Boş geçilemez!");
        setTimeout(function () {
            $(element).tooltip("destroy");
        }, 2000);
        return false;
    }
}

//Girilen değer karakter mi değil mi? Kontrol ediliyor. Karakter değilse gerekli hata mesajları yazdırılıyor.
function isCharacter(evt, el) {
    var element = el;
    if (el.indexOf('.') != 0)//Eğer id değil de sınıf ile input için çalışıyorsa
        element = "#" + el;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 127 || charCode == 8 || charCode == 13) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        $(element).attr("title", "Sadece sayı giriniz!");
        $(element).tooltip("show");
        $(element).attr("title", "Boş geçilemez!");
        setTimeout(function () {
            $(element).tooltip("destroy");
        }, 2000);
        return false;
    }
    else
        return true;
}

/*Yazdırma butonuna basıldığında burası çalışır.
 *Sayfa büyüdüğünde yan tarafa taşınan butonların yazdırma sayfasında görünmesi engelleniyor.*/
function printDiv() {
    $(".onlyPage").hide();
    window.print();
    changeButtons();
}

//Sayfada taşma olduğunda alt kısımdaki butonların küçük halleri görünür yapılıyor.
function changeButtons() {
    if (isOverflow()) {
        $("#sendPrinterButton").stop().fadeTo(400, 0);
        $("#sendPrinterButton").children("i").stop().fadeTo(400, 0);//IE8 uyumluluğu için butonların içerisindeki iconların da ayrı şekilde gizlenmesi gerekti
        $("#sendPrinterButtonSmall").stop().fadeTo(400, 1);
        $("#sendPrinterButtonSmall").children("i").stop().fadeTo(400, 1);

        $("#searchListButton").stop().fadeTo(400, 0);
        $("#searchListButton").children("i").stop().fadeTo(400, 0);
        $("#searchListButtonSmall").stop().fadeTo(400, 1);
        $("#searchListButtonSmall").children("i").stop().fadeTo(400, 1);

        $("#loginButton").stop().fadeTo(400, 0);
        $("#loginButton").children("i").stop().fadeTo(400, 0);
        $("#loginButtonSmall").stop().fadeTo(400, 1);
        $("#loginButtonSmall").children("i").stop().fadeTo(400, 1);
    } else {
        $("#sendPrinterButton").stop().fadeTo(400, 1);
        $("#sendPrinterButton").children("i").stop().fadeTo(400, 1);
        $("#sendPrinterButtonSmall").stop().fadeTo(400, 0);
        $("#sendPrinterButtonSmall").children("i").stop().fadeTo(400, 0);

        $("#searchListButton").stop().fadeTo(400, 1);
        $("#searchListButton").children("i").stop().fadeTo(400, 1);
        $("#searchListButtonSmall").stop().fadeTo(400, 0);
        $("#searchListButtonSmall").children("i").stop().fadeTo(400, 0);


        $("#loginButton").stop().fadeTo(400, 1);
        $("#loginButton").children("i").stop().fadeTo(400, 1);
        $("#loginButtonSmall").stop().fadeTo(400, 0);
        $("#loginButtonSmall").children("i").stop().fadeTo(400, 0);
    }
}

//Sayfada taşma var mı kontrol ediliyor.
function isOverflow() {
    if ($("body").height() > $(window).height())
        return true;
    return false;
}

//Ghost View problemini çözmek için.. Eğer view daha önceden render edilmişse öncekini sil yenisini oluştur diyoruz.
function disposeView(view) {
    Backbone.View.prototype.close = function () {
        this.unbind();
        this.undelegateEvents();
    };

    /* Şu anki viewi yok et */
    if (this.currentView !== undefined) {
        this.currentView.close();
    }

    /* Yeni view oluştur. */
    this.currentView = view;
    this.currentView.delegateEvents();

    return this.currentView;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function goHome() {
    var url = window.location.href;
    window.location.replace(url.substring(0, url.indexOf('?')) + "#");
}